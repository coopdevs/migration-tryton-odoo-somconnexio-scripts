DEFAULT_COUNTRY = "base.es"
DEFAULT_STATE = "base.state_es_b"


class Partner:
    # Tryton fields
    PARTY_FIELDS = [
        "id",
        "id",
        "active",
        "party_type",
        "name",
        "first_name",
        "trade_name",
        "gender",
        "comment",
        "birthday",
        "date_partner",
        "date_partner_end",
        "partner_number",
    ]
    IDENTIFIER_FIELDS = [
        "code",
    ]
    LANGUAGE_FIELDS = [
        "code",
    ]
    PARTY_ADDRESS_FIELDS = [
        "city",
        "zip",
        "country",
        "subdivision",
        "streetbis",
        "street",
        "invoice",
        "sim",
        "delivery"
    ]
    CONTACT_METHOD_FIELDS = [
        "type",
        "value",
    ]

    # Odoo fields
    PARTNER_FIELDS = [
        "id",
        "ref",
        "active",
        "is_company",
        "lastname",
        "firstname",
        "company_name",
        "gender",
        "comment",
        "birthdate",
        "sc_effective_date",
        "sc_cooperator_end_date",
        "sc_cooperator_register_number",
        "vat",  # IDENTIFIER_FIELDS.code
        "lang",  # LANGUAGE_FIELDS.code
        "sponsor_id/id",  # REFERRAL_FIELDS.referrer
        "customer",
        "supplier",
        "email",
        "phone",
        "mobile",
    ]
    PARTNER_ADDRESS_FIELDS = [
        "contacts/city",
        "contacts/zip",
        "contacts/country/id",
        "contacts/state/id",
        "contacts/street2",
        "contacts/street",
        "contacts/type",
    ]
    PARTNER_CONTACT_FIELDS = [
        "contacts/name",
        "contacts/email",
        "contacts/phone",
        "contacts/mobile",
    ]
    PARTNER_BANK_FIELD = [
        "bank_ids/acc_number",
        "bank_ids/bank_id",
    ]

    # Mappers
    STATES = {}
    COUNTRIES = {
        "70": "base.it",  # | Italy
        "46": "base.de",  # | Germany
        "202": "base.nl",  # | Netherlands
        "71": "base.ro",  # | Romania
        "61": "base.us",  # | United States
        "199": "base.ie",  # | Ireland
        "56": "base.ee",  # | Estonia
        "194": "base.at",  # | Austria
        "248": "base.se",  # | Sweden
        "64": "base.es",  # | Spain
        "215": "base.fr"  # | France
    }
    filename = "res.partner-{}.csv"
    csv_fields = (
        PARTNER_FIELDS +
        PARTNER_CONTACT_FIELDS +
        PARTNER_BANK_FIELD +
        PARTNER_ADDRESS_FIELDS
    )

    def __init__(self, config, start, end, id, limit, last_cooperator, group, old_sponsored_cat_id, supplier_cat_id):
        self.config = config
        self.cur = config.cur
        self.start = start
        self.end = end
        self.id = id
        self.limit = limit
        self.last_cooperator = last_cooperator
        self.group = group
        self.old_sponsored_cat_id = old_sponsored_cat_id
        self.supplier_cat_id = supplier_cat_id

    def _transform_partner_data(self, row):
        party_data = {}
        for i in range(len(row)):
            value = row[i]
            field = self.PARTNER_FIELDS[i]
            if field == "member":
                value = True if value is not None else False
            if field == "is_company":
                value = False if value == "person" else True
            if field == "sponsor_id/id":
                if self.group != "old-sponsored":
                    value = "__import__.{}".format(value) if value is not None else False
            value = self._encode_string(value)
            party_data[field] = value
        return party_data

    def _get_address_type(self, row, selected_invoice_address):
        # TODO: Meee! Change this code, please
        invoice = row[6]
        sim = row[7]
        delivery = row[8]
        if invoice and not selected_invoice_address:
            value = "Invoice address"
            selected_invoice_address = True
        elif sim or delivery:
            value = "Shipping address"
        else:
            # Cuantas direcciones de estas hay en prod?
            value = "Other address"
        return value, selected_invoice_address

    def _transform_partner_address_data(self, row, selected_invoice_address):
        address_data = {}
        for i in range(len(self.PARTNER_ADDRESS_FIELDS)):
            value = row[i]
            field = self.PARTNER_ADDRESS_FIELDS[i]
            if field == "contacts/country/id":
                value = self.COUNTRIES.get(str(value), DEFAULT_COUNTRY)
            elif field == "contacts/state/id":
                value = self.STATES.get(str(value), DEFAULT_STATE)
            elif field == "contacts/type":
                value, selected_invoice_address = self._get_address_type(row, selected_invoice_address)
            value = self._encode_string(value)
            address_data[field] = value
        return address_data, selected_invoice_address

    def _transform_partner_contact_method_data(self, row):
        field = row[0]
        value = row[1]
        if field == "email":
            type = "Contract Email"
        else:
            type = "Contact"
        return {
            "contacts/name": value,
            "contacts/{}".format(field): value,
            "contacts/type": type,
        }

    def _get_partner_banks_data(self, party_id):
        # Execute a query searching Party Contact method data
        self.cur.execute("""
        SELECT
            AccountN.number_compact,
            Bank.bic
        FROM "bank_account-party_party" AS PartyA
        INNER JOIN bank_account_number AS AccountN
        ON PartyA.account = AccountN.account
        INNER JOIN bank_account AS BankA
        ON BankA.id = AccountN.account
        INNER JOIN bank AS Bank
        ON Bank.id = BankA.bank
        WHERE PartyA.owner = {};
        """.format(party_id))

        # Retrieve query results
        records = self.cur.fetchall()

        bank_accounts = []
        for r in records:
            bank_accounts.append({
                "bank_ids/acc_number": r[0],
                "bank_ids/bank_id": str(r[1])
            })
        return bank_accounts

    def _get_partner_contact_method_data(self, party_id):
        # Execute a query searching Party Contact method data
        self.cur.execute("""
        SELECT
            {}
        FROM party_contact_mechanism
        WHERE party = {} AND active = TRUE
        AND type in ('email', 'phone', 'mobile')
        """.format(", ".join(self.CONTACT_METHOD_FIELDS), party_id))

        # Retrieve query results
        records = self.cur.fetchall()

        contact_methods = []
        for r in records:
            contact_method_data = self._transform_partner_contact_method_data(r)
            contact_methods.append(contact_method_data)
        return contact_methods

    def _get_partner_addresses_data(self, party_id):
        # Execute a query searching Party Address data
        self.cur.execute("""
        SELECT
            {}
        FROM party_address
        WHERE party = {} AND active = TRUE
        ORDER BY create_date DESC
        """.format(", ".join(self.PARTY_ADDRESS_FIELDS), party_id))

        # Retrieve query results
        records = self.cur.fetchall()

        partner_addresses = []
        selected_invoice_address = False
        for r in records:
            partner_address_data, selected_invoice_address = self._transform_partner_address_data(
                r,
                selected_invoice_address)
            partner_addresses.append(partner_address_data)
        return partner_addresses

    def get_data(self):
        # Prepare Subdivision/State mapping
        self._get_subdivision_data()

        query = self._prepare_query()
        if not query:
            print("\n\t*ERROR*\nWrong partner group.\nType --help to show the help menu.")
            exit(0)
        print(query)
        self.cur.execute(query)
        # Retrieve query results
        records = self.cur.fetchall()

        partners_data = []
        for r in records:
            partner_data = self._transform_partner_data(r)
            phone = None
            email = None
            mobile = None

            r_id = int(partner_data["ref"])

            addresses = self._get_partner_addresses_data(r_id)

            contact_methods = self._get_partner_contact_method_data(r_id)

            banks = self._get_partner_banks_data(r_id)

            # Get first email and first phone
            def_contact_methods = []
            for contact in contact_methods:
                if contact.get('contacts/email') is not None and not email:
                    email = contact.get('contacts/email')
                elif contact.get('contacts/phone') is not None and not phone:
                    phone = contact.get('contacts/phone')
                elif contact.get('contacts/mobile') is not None and not mobile:
                    mobile = contact.get('contacts/mobile')
                else:
                    def_contact_methods.append(contact)

            partner_data["phone"] = phone
            partner_data["mobile"] = mobile
            partner_data["email"] = email
            partners_data.append(partner_data)
            for address in addresses:
                partners_data.append(address)
            for contact in def_contact_methods:
                partners_data.append(contact)
            for bank in banks:
                partners_data.append(bank)
        return partners_data

    def _get_subdivision_data(self):
        # Execute a query searching Subdivisions in use
        self.cur.execute("""
        SELECT
            addr.subdivision
        from party_address AS addr
        INNER JOIN country_subdivision AS su
        ON su.id = addr.subdivision
        WHERE su.type = 'province'
        GROUP BY addr.subdivision
        """)

        # Retrieve query results
        list_of_subdiv = self.cur.fetchall()
        sub_list = [str(d[0]) for d in list_of_subdiv]
        # Execute a query searching Partner data
        self.cur.execute("""
        SELECT
            id, code
        FROM country_subdivision
        WHERE id IN ({})
        """.format(", ".join(sub_list)))

        # Retrieve query results
        subdivisions = self.cur.fetchall()

        for r in subdivisions:
            code = "base.state_{}".format(r[1].lower().replace("-", "_"))
            self.STATES[str(r[0])] = code

    def _encode_string(self, value):
        if isinstance(value, str):
            # TODO: Fix the codification:
            # With and encode/decode we recive the next error:
            # UnicodeDecodeError: 'ascii' codec can't decode byte 0xc2
            # in position 12: ordinal not in range(128)
            return value
        return value

    def _prepare_query(self):
        if self.group == 'cooperator':
            query = self._prepare_cooperator_query()
        elif self.group == 'ex-cooperator':
            query = self._prepare_ex_cooperator_query()
        elif self.group == 'sponsored':
            query = self._prepare_new_sponsored_query()
        elif self.group == 'old-sponsored':
            query = self._prepare_old_sponsored_query()
        elif self.group == 'supplier':
            query = self._prepare_providers_query()
        else:
            return
        if self.start:
            query = "{} AND Party.ID > {}".format(
                query,
                self.start)
        if self.end:
            query = "{} AND Party.ID < {}".format(
                query,
                self.end)
        if self.id:
            query = "{} AND Party.ID = {}".format(query, self.id)
        query = """
        {}
        ORDER BY Party.id ASC
        """.format(query)
        if self.limit:
            query = "{} LIMIT {}".format(self.limit)
        return query

    def _prepare_cooperator_query(self):
        # Prepare the select fields
        party_fields = ["Party.{}".format(field) for field in self.PARTY_FIELDS]
        identifier_fields = ["Identifier.{}".format(field) for field in self.IDENTIFIER_FIELDS]
        lang_fields = ["Language.{}".format(field) for field in self.LANGUAGE_FIELDS]
        referral_fields = ["NULL AS referrer"]
        customer_supplier_fields = ["TRUE AS customer, FALSE AS supplier"]

        query = """
        SELECT
            {}
        FROM party_party AS Party
        LEFT JOIN party_identifier AS Identifier
        ON (Party.id = Identifier.party)
        LEFT JOIN ir_property AS Property
        ON (
          Property.field = 970 AND
          Property.res = ('party.party,' || Party.id)
        )
        LEFT JOIN ir_lang AS Language
        ON (('ir.lang,' || Language.id) = Property.value)
        WHERE Party.active = TRUE AND
        Party.partner_number IS NOT NULL AND
        Party.date_partner_end IS NULL
        """.format(
            ", ".join(party_fields + identifier_fields + lang_fields + referral_fields + customer_supplier_fields),
        )
        return query

    def _prepare_ex_cooperator_query(self):
        # Prepare the select fields
        party_fields = ["Party.{}".format(field) for field in self.PARTY_FIELDS]
        identifier_fields = ["Identifier.{}".format(field) for field in self.IDENTIFIER_FIELDS]
        lang_fields = ["Language.{}".format(field) for field in self.LANGUAGE_FIELDS]
        referral_fields = ["NULL AS referrer"]
        customer_supplier_fields = ["TRUE AS customer, FALSE AS supplier"]
        query = """
        SELECT
            {}
        FROM party_party AS Party
        LEFT JOIN party_identifier AS Identifier
        ON (Party.id = Identifier.party)
        LEFT JOIN ir_property AS Property
        ON (
          Property.field = 970 AND
          Property.res = ('party.party,' || Party.id)
        )
        LEFT JOIN ir_lang AS Language
        ON (('ir.lang,' || Language.id) = Property.value)
        WHERE Party.active = TRUE AND
        Party.date_partner_end IS NOT NULL
        """.format(
            ", ".join(party_fields + identifier_fields + lang_fields + referral_fields + customer_supplier_fields),
        )
        return query

    def _prepare_new_sponsored_query(self):
        # Prepare the select fields
        party_fields = ["Party.{}".format(field) for field in self.PARTY_FIELDS]
        identifier_fields = ["Identifier.{}".format(field) for field in self.IDENTIFIER_FIELDS]
        lang_fields = ["Language.{}".format(field) for field in self.LANGUAGE_FIELDS]
        referral_fields = ["Referral.referrer"]
        customer_supplier_fields = ["TRUE AS customer, FALSE AS supplier"]
        query = """
        SELECT
            {}
        FROM party_party AS Party
        LEFT JOIN party_identifier AS Identifier
        ON (Party.id = Identifier.party)
        LEFT JOIN ir_property AS Property
        ON (
          Property.field = 970 AND
          Property.res = ('party.party,' || Party.id)
        )
        LEFT JOIN ir_lang AS Language
        ON (('ir.lang,' || Language.id) = Property.value)
        INNER JOIN referral as Referral
        ON (
          Referral.referred = Party.id AND
          Referral.referrer_code IS NULL
        )
        WHERE Party.active = TRUE AND
        Party.partner_number IS NULL AND
        Party.date_partner_end IS NULL
        """.format(
            ", ".join(party_fields + identifier_fields + lang_fields + referral_fields + customer_supplier_fields),
        )
        if self.last_cooperator:
            query = "{} AND Referral.referrer <= {}".format(
                query,
                self.last_cooperator)
        return query

    def _prepare_old_sponsored_query(self):
        # Prepare the select fields
        party_fields = ["Party.{}".format(field) for field in self.PARTY_FIELDS]
        identifier_fields = ["Identifier.{}".format(field) for field in self.IDENTIFIER_FIELDS]
        lang_fields = ["Language.{}".format(field) for field in self.LANGUAGE_FIELDS]
        referral_fields = ["'base.main_partner' AS referrer"]
        customer_supplier_fields = ["TRUE AS customer, FALSE AS supplier"]
        query = """
        SELECT
            {}
        FROM party_party AS Party
        INNER JOIN party_category_rel AS PartyCat
        ON (Party.id = PartyCat.party AND PartyCat.category = {})
        LEFT JOIN party_identifier AS Identifier
        ON (Party.id = Identifier.party)
        LEFT JOIN ir_property AS Property
        ON (
          Property.field = 970 AND
          Property.res = ('party.party,' || Party.id)
        )
        LEFT JOIN ir_lang AS Language
        ON (('ir.lang,' || Language.id) = Property.value)
        WHERE Party.active = TRUE AND
        Party.partner_number IS NULL AND
        Party.date_partner_end IS NULL
        """.format(
            ", ".join(party_fields + identifier_fields + lang_fields + referral_fields + customer_supplier_fields),
            self.old_sponsored_cat_id,
        )
        return query

    def _prepare_providers_query(self):
        # Prepare the select fields
        party_fields = ["Party.{}".format(field) for field in self.PARTY_FIELDS]
        identifier_fields = ["Identifier.{}".format(field) for field in self.IDENTIFIER_FIELDS]
        lang_fields = ["Language.{}".format(field) for field in self.LANGUAGE_FIELDS]
        referral_fields = ["NULL AS referrer"]
        customer_supplier_fields = ["FALSE AS customer, TRUE AS supplier"]
        query = """
        SELECT
            {}
        FROM party_party AS Party
        INNER JOIN party_category_rel AS PartyCat
        ON (Party.id = PartyCat.party AND PartyCat.category = {})
        LEFT JOIN party_identifier AS Identifier
        ON (Party.id = Identifier.party)
        LEFT JOIN ir_property AS Property
        ON (
          Property.field = 970 AND
          Property.res = ('party.party,' || Party.id)
        )
        LEFT JOIN ir_lang AS Language
        ON (('ir.lang,' || Language.id) = Property.value)
        WHERE Party.active = TRUE AND
        Party.partner_number IS NULL AND
        Party.date_partner_end IS NULL
        """.format(
            ", ".join(party_fields + identifier_fields + lang_fields + referral_fields + customer_supplier_fields),
            self.supplier_cat_id,
        )
        return query
