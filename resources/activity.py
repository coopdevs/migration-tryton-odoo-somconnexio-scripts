class Activity:
    ODOO_ACTIVITY_FIELDS = [
        "id",
        "summary",
        "activity_type_id/id",
        "location",
        "date_deadline",
        "date_done",
        "done",
        "res_id",
        "note",
        "user_id/id",
        "res_model/id",
    ]
    TRYTON_ACTIVITY_FIELDS = [
        "id",
        "subject",
        "activity_type",
        "location",
        "dtstart",
        "dtend",
        "state",
        "resource",
        "description",
        "create_uid"
    ]
    filename = "mail.activity-{}.csv"
    csv_fields = (
        ODOO_ACTIVITY_FIELDS
    )

    def _prepare_query_partner(self):
        activity_fields = [
            "Activity.{}".format(field)
            for field in self.TRYTON_ACTIVITY_FIELDS
        ]
        model_fields = [
            "'base.model_res_partner' AS Modelo"
        ]
        query = """
        SELECT
            {}
        FROM activity_activity AS Activity
        WHERE Activity.resource LIKE 'party.party,%' 
        """.format(
            ", ".join(activity_fields+model_fields)
        )
        return query

    def _prepare_query_contract(self):
        activity_fields = [
            "Activity.{}".format(field)
            for field in self.TRYTON_ACTIVITY_FIELDS
        ]
        model_fields = [
            "'contract.model_contract_contract' AS Modelo"
        ]
        query = """
        SELECT
            {}
        FROM activity_activity AS Activity
        WHERE Activity.resource LIKE 'contract,%' 
        """.format(
            ", ".join(activity_fields+model_fields)
        )
        return query

    def _prepare_query(self):
        if self.group == 'partner':
            query = self._prepare_query_partner()
        elif self.group == 'contract':
            query = self._prepare_query_contract()
        else:
            return
        if self.start:
            query = "{} AND Activity.ID > {}".format(
                query,
                self.start
            )
        if self.end:
            query = "{} AND Activity.ID < {}".format(
                query,
                self.end
            )
        if self.id:
            query = "{} AND Activity.ID = {}".format(query, self.id)
        query = """
        {}
        ORDER BY Activity.id ASC
        """.format(query)
        if self.limit:
            query = "{} LIMIT {}".format(query, self.limit)
        return query

    def __init__(self, config, start, end, id, limit, _, group, *args):
        self.id = id
        self.config = config
        self.cur = config.cur
        self.group = group
        self.start = start
        self.end = end
        self.limit = limit

    def get_data(self):
        # Prepare Subdivision/State mapping
        query = self._prepare_query()
        if not query:
            print("\n\t*ERROR*\nWrong activity group.\nType --help to show the help menu.")
            exit(0)
        print(query)
        self.cur.execute(query)
        # Retrieve query results
        records = self.cur.fetchall()
        activities_data = []
        for r in records:
            activities_data.append(self._transform_activity_data(r))
        return activities_data

    def _transform_activity_data(self, row):
        activity_data = {}
        for i in range(len(row)):
            value = row[i]
            field = self.ODOO_ACTIVITY_FIELDS[i]
            if field == "id":
                value = "__import__.mail_activity_"+str(value)
            elif field == "activity_type_id/id":
                value = "__import__.mail_activity_type_"+str(value)
            elif field == "user_id/id":
                if value not in (
                    [6, 7, 8, 9, 12, 14, 30, 41, 43, 44, 51, 52, 60, 63] +
                    [64, 65, 66, 67, 68, 70, 71, 72]
                ):
                    value = "base.user_admin"
                else:
                    value = "__import__.res_users_"+str(value)
            elif field in ["date_deadline", "date_done"]:
                value = value.strftime("%Y-%m-%d") if value is not None else False
            elif field == "res_id":
                value = value[value.find(",")+1:]
            elif field == "done":
                value = value == "held"
            elif field == "note":
                value = "<pre>{}</pre>".format(value)
            activity_data[field] = value
        return activity_data
