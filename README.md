# Migration Tryton-Odoo SomConnexio scripts

This project contains the scripts used to migrate the SomConnexio data from Tryton to Odoo.

## Import strategy

1. Extract the CSVs from Tryton DB.
2. Import Bank data (we only need this to update Banca Popolare Etica)
3. Import cooperators data
4. Import ex-cooperators data
5. Import sponsored data
6. Import providers
7. Create the coop agreements
8. Import the special sponsored users (needs the coop_agreement)
9. Execute membership script
10. Convert Activities' `res_id` column from Tryton ids to Odoo ones
11. Import Activities

## Extract the CSVs

### Script migration state

We need to migrate:

- [X] Partners
- [ ] Contracts
- [X] Activities
- [ ] Accounting?

### Usage

1. Clone the repository in a server with the Tryton DB.
2. Install the package: `pip install -e .` inside the repository folder.
3. Create a config file with the DB configuration.
3. Execute the command `extract` with the `resource` that you want to extract data.
4. Import the resultant CSV to Odoo.

#### DB Config

Create a config file named `config.ini` with the next content:

```ini
[DB]
name = my_db
user = my_user
port = 5555
host = localhost
```

#### `extract` command usage examples

* Extract help
```sh
$ extract --help
```

* Extract the first 100 cooperators
```sh
$ extract --limit 100 partner
```

* Extract the first 100 sponsored (new sponsored)
```sh
$ extract --limit 100 partner sponsored
```

### Partners

#### Data

- [X] Name, etc.
- [X] Identificator
- [X] Addresses
- [X] Language
- [X] Email
- [X] Phone
- [X] Bank Accounts (we need to investigate how to import the banks)
- [X] Membership

#### Groups

- [X] Cooperators

```bash
$ extract partner cooperator
```

- [X] ExCooperators

```bash
$ extract partner ex-cooperator
```

- [X] Sponsored

```bash
$ extract partner sponsored
```

- [X] Old Sponsored

```bash
$ extract partner old-sponsored --old-sponsored-cat-id 11
```

- [X] Suppliers

```bash
$ extract partner suppliers --supplier-cat-id 12
```

### Activities

#### Data

- [X] Summary
- [X] Origin
- [X] Record
- [X] State
- [X] Due Date
- [X] Done Date  
- [X] Location
- [X] User  
- [X] Note

#### Groups

- [X] Contacts' Activities

```bash
$ extract activity partner
```  
- [X] Contracts' Activities

```bash
$ extract activity contract
```  

## Partners membership script

Using the [`erppeek`](https://erppeek.readthedocs.io/en/latest/) client we created a script to manage the membership logic if EMC in Odoo.
Using the cooperator number and cooperator date of Tryton, we update the cooperator partners.

### Usage

1. Clone the repository in a server with the Odoo app running.
1. Go to `erppeek` folder.
2. Install the requirements: `pip install -r requirements.txt`.
3. Create a config file with the Odoo configuration.
3. Execute the script `python erppeek-script.py`.

## Activities conversion res_id

### Usage

1. Clone the repository in a server with the Odoo app running.
2. Go to `erppeek` folder.
3. Install the requirements: `pip install -r requirements.txt`.
4. Create a config file with the Odoo configuration.
5. Copy Partner's Activities CSV to `../data/mail.activity-partner.csv`
6. Execute the script `python activity_partner_ref_2_id.py`
7. The final CSV is in `../data/mail.activity-partner_Odoo.csv`
8. Copy Contract's Activities CSV to `../data/mail.activity-contract.csv`
9. Execute the script `python activity_contract_code_2_id.py`
10. The final CSV is in `../data/mail.activity-contract_Odoo.csv`
#### Odoo Config

Create a config file named `erppeek.ini` with the next content:

```ini
[DEFAULT]
scheme = http
host = localhost
port = 8069
database = odoo
username = admin

[odoo]
password = admin
protocol = xmlrpc
```

## Fix partners membership scrip

The first version of membership script has a bug and modify all the partners with an int (also 0) in the auxiliar member number field.
This script fix the data affected by this bug.
If in our data you can see members with sponsorship type and the Efective Cooperator flag True, you can execute this script to fix this bugged data:

### Usage

1. Clone the repository in a server with the Odoo app running.
1. Go to `erppeek` folder.
2. Install the requirements: `pip install -r requirements.txt`.
3. Create a config file with the Odoo configuration.
3. Execute the script `python erppeek-fix-sponsored.py`.

