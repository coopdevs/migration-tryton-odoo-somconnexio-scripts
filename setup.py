from setuptools import setup


setup(
        name='Migration scripts Tryton-Odoo SomConnexio',
        version='1.0',
        py_modules=['main'],
        install_requires=[
            'Click',
        ],
        entry_points='''
            [console_scripts]
            extract=main:extract_data
        '''
)
