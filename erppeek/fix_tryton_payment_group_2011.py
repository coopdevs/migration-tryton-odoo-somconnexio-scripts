# -*- coding: utf-8 -*-
import erppeek
import sys
import csv

try:
    csv_file = sys.argv[1]
except:
    exit("""
ERROR! A CSV with the details of the payment moves is required.

Run:
    python fix_tryton_payment_group_2011.py <CSV Account moves path>

    """)

# Read CSV and create an object with all the data needed.
print("* Read CSV and create an object with all the data needed.")
account_move_lines_raw_data = []
with open(csv_file, mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=";")
    for row in csv_reader:
        account_move_lines_raw_data.append(row)
        group = row["Referència"]

client = erppeek.Client.from_config('odoo')

AccountMove = client.model("account.move")
AccountAccount = client.model("account.account")
Journal = client.model("account.journal")
ResPartner = client.model("res.partner")

remesa_journal = Journal.get([("code", "=", "REM")])
account431 = AccountAccount.get([("code", "=", "43100000")])
account430 = AccountAccount.get([("code", "=", "43000000")])

new_move_data = {
    "journal_id": remesa_journal.id,
    "date": "2021-03-24",
    "ref": "Orden de Cobro PAY0106 Grupo de Tryton {}".format(group),
    "move_type": "other",
    "line_ids": []
}

def create_430(partner, amount, details):
    return {
        "name": details,
        "account_id": account430.id,
        "partner_id": partner.id,
        "debit": amount,
        "credit": 0,
    }

def create_431(amount):
    return {
        "account_id": account431.id,
        "debit": 0,
        "credit": amount,
    }

total_amount = 0
for row in account_move_lines_raw_data:
    partner = ResPartner.get([("ref", "=", row["Tercer/ID"])])
    amount = float(row["Pagaments/Import"])
    details = row["Pagaments/Descripció"]
    new_move_data["line_ids"].append((0, 0, create_430(partner, amount, details)))
    total_amount = total_amount + amount

new_move_data["line_ids"].append((0, 0, create_431(total_amount)))

new_move = AccountMove.create(new_move_data)

print(new_move.id)
