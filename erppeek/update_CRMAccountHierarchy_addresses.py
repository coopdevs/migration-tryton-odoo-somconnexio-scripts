import erppeek
import csv
from pyopencell.resources.subscription import Subscription
from pyopencell.resources.crm_account_hierarchy import CRMAccountHierarchy
from pyopencell.exceptions import PyOpenCellAPIException


def lang(lang_code):
    if lang_code == 'es_ES':
        return 'ESP'
    elif lang_code == 'ca_ES':
        return 'CAT'


client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")

crm_account_hierarchies_to_update = []

with open('OC_codes.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        crm_account_hierarchies_to_update.append(row.get("code"))

for crm_code in crm_account_hierarchies_to_update:
    partner_ref = crm_code.split("_")[0]
    partner = Partner.browse([
        ("ref", "=", partner_ref)
    ]).read(
        "city zip street state_id lang"
    )
    if len(partner) > 1:
        print("More than one partner found with reference {}.".format(partner_ref))
        continue
    partner = partner[0]
    try:
        address = {
            "address1": partner["street"],
            "zipCode": partner["zip"],
            "city": partner["city"],
            "state": partner["state_id"].name,
            "country": "ES",
         }
    except AttributeError as error:
        print("KO, parnter_ref {} without complete address".format(partner_ref))
        continue

    try:
        CRMAccountHierarchy.update(
            code=crm_code,
            address=address,
            crmAccountType="CA_UA",
            language=lang(partner["lang"]),
            currency="EUR",
        )
        print("OK, {}".format(crm_code))
    except PyOpenCellAPIException as error:
        print("KO, {}".format(crm_code))
