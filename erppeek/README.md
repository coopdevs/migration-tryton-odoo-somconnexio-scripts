## ERPPeek migration scripts

This directory contains the migration scripts used in the SomConnexió ERP migration project.
Using the [`erppeek`](https://erppeek.readthedocs.io/en/latest/) client we created a script to help in the Tryton-Odoo migration.

#### Configuration

Create a config file named `erppeek.ini` with the next content:

```ini
[DEFAULT]
scheme = http
host = localhost
port = 8069
database = odoo
username = admin

[odoo]
password = admin
protocol = xmlrpc
```

### Usage

0. Access to the server with SSH and follow the ssh-agent:
```
ssh -A user@host
```
1. Clone the repository in a server with the Odoo app running.
```
git@gitlab.com:coopdevs/migration-tryton-odoo-somconnexio-scripts.git
```
If the repository already exists, pull the new changes:
```
cd ~/migration-tryton-odoo-somconnexio-script
git pull
```
2. Go to `migration-tryton-odoo-somconnexio-script/erppeek` folder.
```
cd ~/migration-tryton-odoo-somconnexio-script/erppeek
```
3. Modify the `erppeek.ini` with the Odoo credentials. Use the user and password to access the Odoo via UI.
4. Change to `odoo` user:
```
sudo su odoo
```
5. Activate the virtual env
```
pyenv activate odoo
```
6. Install the requirements:
```
pip install -r requirements.txt
```
7. Run the script instructions

### Scripts

#### Force membership for imported partners - `erppeek-script.py` and `erppeek-fix-sponsored.py`

Two scripts, one for the migration and another for a fix in the migration.
This script sets the cooperator effective number and effective date using the auxiliar fields with the migrated data.
The fix script loops over the sponsorships to remove the flag member setted by the first script.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script `python erppeek-script.py`.
2. Execute the script `python erppeek-fix-sponsored.py`.

#### Activities Conversion res_id - `activity_partner_ref_2_id.py`, `activity_odoo_res_id_get.py` and `activity_contract_code_2_id.py`.

**Usage**

0. Follow the instructions of Usage section.
1. Copy Partner's Activities CSV to `../data/mail.activity-partner.csv`
2. Execute the script `python activity_partner_ref_2_id.py`
3. The final CSV is in `../data/mail.activity-partner_Odoo.csv`
4. Copy Contract's Activities CSV to `../data/mail.activity-contract.csv`
5. Execute the script `python activity_contract_code_2_id.py`
6. The final CSV is in `../data/mail.activity-contract_Odoo.csv`

#### Fix old members data - `create-subcription-register-ex-cooperator.py`

Easy my coop uses the Subscription Register to mantain a register of cooperators. Using this register we can get the end date of ex-cooperators. Searching for the last entry in SubscriptionRegister for partner X with type sell back we can check the end date.
This script creates a Subscription Register type sell back for every old member migrated.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script `python create-subcription-register-ex-cooperator.py`.

#### Fill partners without address with related address data - `parnter_fill_address.py`

Many partners were migrated from Tryton to Odoo without setting their address attributes (`street`, `street2`, `zip`, `city`, `state_id`, `country_id`). Using this register we can fill these attributes within the partner's related addresses. Searching all partner child addresses and selecting the most appropiate if several found (invoice > service > other > delivery) we retrieve the information to fill the parent address fields.
This script updates a partner with address information if it had none, given that it has related addresses.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs `python partner_fill_address.py > /home/odoo/partner_fill_address.log`.

#### Force recompute partner_id of MailActivities related with AccountInvoices - `recompute_partner_id_in_account_invoice.py`

We need to show all the activities related with a Partner from the Partner view (related with the same partner, its addresses, its contracts or its invoices).

Until now, we had the relationship between activities and partner, address and contract, but not of invoice.

This script modifies res_model field of filtered activities to force partner_id re-computation.

The re-computation gets the partner id related to account invoice and trespasses it to activity model.

Related with: https://gitlab.com/coopdevs/odoo-somconnexio/-/merge_requests/406

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script `python recompute_partner_id_in_account_invoice.py`.


#### Update CRMHiearchies addresses with their corresponding partner adresss - update_CRMAccountHierarchy_addresses.py`

Some Odoo calls to OC deleted the address attributes from some Client CRMHierarchies.
This script updates all CRMHierarchies from CustomerAccount to User Account with the corresponding partner address in odoo.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs `python update_CRMAccountHierarchy_addresses.py > /home/odoo/update_CRMAccountHierarchy_addresses.log`.


#### Fix old members converted to members - `fix_converted_old_members.py`
##### Fix cooperator flag in migrated partner

All the partners migrated from Tryton that are member, old_member or coop_candidate hasn't the flag `cooperator` set to TRUE.
This flag is required to start the membership process (add coop register number, set cooperator start date and change the flag coop candidate).

To update this bad migrated Partners we can run the next SQL query:

```
> UPDATE res_partner SET cooperator = TRUE  WHERE cooperator IS FALSE AND (coop_sponsee IS TRUE OR member IS TRUE OR old_member IS TRUE OR coop_candidate IS TRUE OR coop_agreement IS TRUE) AND customer IS TRUE AND parent_id IS NULL;
```

##### Move migrated cooperator register number to correct field - `migrate_cooperator_number.py`

In the migration process, we create three fields in ResPartner to save the membership related values: cooperator register number, start date and end date.

For all the members we run a script to copy the value of the auxiliary field to the generic field, but we don't migrate the old members coop number.

To fix this migration error, we can execute the script `migrate_cooperator_number.py` using a CSV with the list of ex members without cooperator register number in Odoo. We can find this CSV in NextCloud with name `Old members without cooperator number_filled 210429.csv`.

**Usage**

0. Follow the instructions of Usage section.
1. Move the `Old members without cooperator number_filled 210429.csv` to the `erppeek` folder.
2. Execute the script and save the logs `python migrate_cooperator_number.py > /home/odoo/migrate_cooperator_number.log`.

##### Fix old members converted to members - `fix_converted_old_members.py`


To fix the partners already affected by the membership process without the `cooperator` flag, we need to create/update the next records:

* Update the partners fields:
  * `member` to `TRUE`
  * `old_member` to `FALSE`
  * `coop_candidate` to `FALSE`
* Create a SubscriptionRegister with the partner and share info.
* Create a ShareLine with the share of partner.

This script uses a CSV to search the partners affected.
To generate the CSV:

1. Run the next SQL query:
```sql
odoo=> \copy (SELECT
    partner.id AS id,
    invoice.date_invoice AS effective_date
FROM res_partner AS partner
INNER JOIN subscription_request AS sr
    ON sr.partner_id = partner.id
INNER JOIN account_invoice AS invoice
    ON sr.id = invoice.subscription_request
WHERE
    partner.sc_effective_date IS NOT NULL AND
    partner.sc_cooperator_end_date IS NOT NULL AND
    partner.sc_cooperator_register_number IS NOT NULL AND
    sr.state = 'done')
TO '/home/odoo/old_members_converted.csv' csv header;
```

**Usage**

0. Follow the instructions of Usage section.
1. Move the `old_members_converted.csv` to the `erppeek` folder.
2. Execute the script and save the logs `python fix_converted_old_members.py > /home/odoo/fix_converted_old_members.log`.


#### Import adsl contract information - `import_adsl_contract_info.py`

This is a script that imports adsl contract information, updating the already existing contracts, locating them by the field `ref`.

The following fields get updated using a csv file named `adsl_contract_info_to_update.csv`.
- id_order
- administrative_number
- ppp_user (only if there is a loaded_ppp_user)
- ppp_password

0. Follow the instructions of Usage section.
1. Copy the `adsl_contract_info_to_update.csv` (founded in a Trello card) to the `erppeek` folder:
```
scp adsl_contract_info_to_update.csv user@host:migration-tryton-odoo-somconnexio-scripts/erppeek/.
```
2. Execute the script and save the logs
```
python import_adsl_contract_info.py > /var/log/odoo/import_adsl_contract_info.log
```

#### Recompute effective date in ResPartner - `recompute_partner_effective_date.py`

This is a script that force the recomputation of computed field effective date. We changed the compute method, so we need to force its recomputation because it is a stored computed field.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python recompute_partner_effective_date.py > /var/log/odoo/recompute_partner_effective_date.log
```

#### Create the relation between the coop agreement and the partners sponsored with coop agreement (MO, MAS) - `create_relation_partner_coop_agreement.py`

We miss one part of the general migration, the relation between the coop agreements and the partners sponsored by a coop.
With the next steps, we migrate the relations from Tryton to Odoo.

**Usage**

First, we must create the coop agreements in Odoo:

1. Open Odoo UI.
2. Go to `Easy-My-Coop > Contacts > Coop Agreements`.
3. Create two coop agreements with the codes used: `MO` and `MAS`.

Once the coop agreements are created, we can export the data from Tryton:

1. Access to the Tryton server.
2. Access to the Tryton DB.
3. Execute:
```sql
\copy (
        select
                c.code as code,
                r.referred as ref
        from referral as r
        inner join referrer as c
        on r.referrer_code = c.id
        order by c.code
) to 'partner-coop_agreement.csv' csv header;
```

And then run the script:

0. Follow the instructions of Usage section.
1. Using scp copy the SQL output file in the odoo server:
```
scp partner-coop_agreement.csv user@host:migration-tryton-odoo-somconnexio-scripts/erppeek/.
```
2. Execute the script and save the logs
```
python recompute_partner_effective_date.py > /var/log/odoo/recompute_partner_effective_date.log
```
#### Recompute name in Contract - `recompute_contract_name.py`

This is a script that force the recomputation of contract's name. This field is now computed. We force it rewritting the field it gets the value from: `phone_number`

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python recompute_contract_name.py > /var/log/odoo/recompute_contract_name.log
```
#### Set catalog attributes - `set_catalog_name.py`

This is a script that sets the field `catalog_name` for all the attributes that have info used by the catalog API. Also we add an attribute to Product Template with implicit info shared by all the variants.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python set_catalog_name.py > /var/log/odoo/set_catalog_name.log
```

#### Fix account moves imported from Tryton

This is a script that creates a AccountMove to move all the debt imported from Tryton to the correct Account.

You need to pass the ID of the AccountMove to fix. For this AccountMove:

* Generate a new AccountMove with the same data.
* For any AccountMoveLine in the AccountMove with the account 430:
  * Generate an AccountMoveLine with the same data but with the account 44000001.
  * Generate an AccountMoveLine with the same data but with the credit and debit inverted.

With this AccountMove we move the debt from the 430 to the 44000001.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python migrate_account_moves_payments_imported_tryton.py 1234 > /var/log/odoo/migrate_account_moves_payments_imported_tryton.log
```

#### Migrate DiscoveryChannel from Tryton to Odoo Partners - `migrate_partner_discovery_channel_from_tryton.py`

This is a script that set the DiscoveryChannel in the model Partner from the CSV extracted from Tryton.

**Usage**

0. Follow the instructions of Usage section.
1. Using scp copy the CSV file in the odoo server:
```
scp tryton_partners_discovery_channel.csv user@host:migration-tryton-odoo-somconnexio-scripts/erppeek/.
```
2. Execute the script and save the logs
```
python migrate_partner_discovery_channel_from_tryton.py > /var/log/odoo/migrate_partner_discovery_channel_from_tryton.log
```

#### Migrate address change flag to type selector - `migrate_address_change_flag.py`

This script migrates change_address boolean field to type selector field. We changed the indicator for a more general one.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python migrate_address_chenge_flag.py > /var/log/odoo/migrate_address_change_flag.log
```

#### Fix the migrated Tryton payment groups in batch - `fix_migrated_account_moves_payments_imported_tryton.py`

This script fix the payment groups imported from Tryton in batch. For any payment group we need to create an AccountMove with the lines moving the debt from 430 account to 44000001.

It's the continuation of Fix account moves imported from Tryton.

**Usage**

0. Follow the instructions of Usage section.
1. Execute the script and save the logs
```
python fix_migrated_account_moves_payments_imported_tryton.py ../../grups\ cobrament\ tryton/<CSV file>.csv
```

#### Set fiber signal type for broadband contracts -`set_fiber_signal_type_ba_contracts.py`

This script searches broadband contracts by their phone_number and sets their fiber_signal_type parameter as indicated in an external csv.

**Usage**

1. Follow the instructions of Usage section.
2. Execute the script and save the logs
```
python set_fiber_signal_type_ba_contracts.py > /var/log/odoo/set_fiber_signal_type_ba_contracts.log
```

#### Substitute mobile products from current contract lines in mobile contracts -`substitute_products_from_mobile_current_tariffs.py`

This script searches mobile contracts by their reference code and phone number and substitutes the product of the contract line representing their current tariff.
We use this to substitute obsolete mobile products from tryton migrated mobile contracts.

**Usage**

1. Follow the instructions of Usage section.
2. Execute the script and save the logs
```
python substitute_products_from_mobile_current_tariffs.py > /var/log/odoo/substitute_products_from_mobile_current_tariffs.log
```
