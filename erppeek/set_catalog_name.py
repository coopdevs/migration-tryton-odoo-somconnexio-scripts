import erppeek

client = erppeek.Client.from_config('odoo')

Attribute = client.model("product.attribute.value")
Template = client.model("product.template")

attrs_data = {
    'T-Conserva': 'CONSERVA',
    'Sense Minuts': '0',
    '100 Min': '100',
    '150 Min': '150',
    '200 Min': '200',
    '300 Min': '300',
    'Trucades Il·limitades': 'UNL',
    'Sense Dades': '0',
    '200 MB': '200',
    '500 MB': '500',
    '1 GB': '1024',
    '2 GB': '2048',
    '3 GB': '3072',
    '5 GB': '5120',
    '10 GB': '10240',
    '23 GB': '23552',
    '30 GB': '30720',
    '50 GB': '51200',
    '100 min a fix i mòbil': '100',
    '1000 min a fix': '1000',
    'Sense telèfon fix': 'SF',
    '100 Mb': '100',
    '600 Mb': '600',
    '1 Gb': '1000',
}

tmpls_data = {
    'somconnexio.150Min1GB_product_template': 'somconnexio.150Min',
    'somconnexio.SenseMinuts1GB_product_template': 'somconnexio.SenseMinuts',
    'somconnexio.TrucadesIllimitadesSenseDades_product_template': 'somconnexio.TrucadesIllimitades',
    'somconnexio.100MinSenseDades_product_template': 'somconnexio.100Min',
    'somconnexio.200MinSenseDades_product_template': 'somconnexio.200Min',
    'somconnexio.FibraGretaSenseMinuts600Mb_product_template': 'somconnexio.600 Mb',
    'somconnexio.SenseFixFibra100Mb_product_template': 'somconnexio.SenseFix'
}
for name in attrs_data:
    print("Set catalog_name {} for attribute {}".format(name, attrs_data[name]))
    attr = Attribute.get([('name', '=', name)])
    attr.catalog_name = attrs_data[name]

for xml_id in tmpls_data:
   print("Set catalog_attribute_id {} for template {}".format(xml_id, tmpls_data[xml_id]))
   attr = Attribute.get(tmpls_data[xml_id])
   tmpl = Template.get(xml_id).catalog_attribute_id = attr
