import erppeek
import csv

client = erppeek.Client.from_config('odoo')

Contracts = client.model("contract.contract")
Products = client.model("product.product")


with open('mobile_contracts_with_products_to_update.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        contract_code = row['contract_code']
        phone_number = row['phone_number']
        new_product_code = row['new_product']

        print("Processing line: ", phone_number)

        contract = Contracts.browse(
            [
                ('code', '=', contract_code),
                ('phone_number', '=', phone_number),
                ('date_end', '=', False),
            ]
        )

        if not contract:
            print("The contract with reference: {} and phone number: {} \
                is either terminated or does not exist in the database".format(
                contract_code, phone_number)
            )
            continue

        new_product = Products.browse(
            [
                ('default_code', '=', new_product_code),
            ],
            limit=1

        )

        if not new_product:
            print("This product is not in the database. ", new_product_code)
            continue
        else:
            new_product = new_product[0]

        current_contract_line = contract.current_tariff_contract_line
        current_contract_line.write({"product_id": new_product.id})

        print("Current tariff changed to: ",
              current_contract_line.product_id.showed_name)
        print("Finished processing line: ", phone_number)
