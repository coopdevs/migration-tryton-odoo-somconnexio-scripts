import erppeek

import csv

client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")
CoopAgreement = client.model("coop.agreement")

mas_coop_agreement, = CoopAgreement.browse(
    [
        ("code", "=", "MAS"),
    ],
    limit=1
)
mo_coop_agreement, = CoopAgreement.browse(
    [
        ("code", "=", "MO"),
    ],
    limit=1
)
coop_agreements = {
    "MAS": mas_coop_agreement.id,
    "MO": mo_coop_agreement.id
}

# Read CSV and copy/consume the number in the sc_cooperator_register_number.
print("* Read CSV with the partners to relate with the coop agreements:")
with open('partner-coop_agreement.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        partner_ref = row.get("ref")
        coop_agreement_code = row.get("code")
        print("* Procesing partner {} with {}:".format(partner_ref, coop_agreement_code))
        partner = Partner.browse([("ref", "=", partner_ref)], limit=1)
        if not partner:
            print("Partner {} NOT FOUND".format(partner_ref))
            continue
        partner = partner[0]
        if partner.member:
            print("Partner {} not processed. Is MEMBER".format(partner_ref))
            continue
        if not partner.coop_sponsee:
            print("Partner {} not processed. Is NOT SPONSORED".format(partner_ref))
            continue
        vals = {
            "coop_agreement_id": coop_agreements[coop_agreement_code],
            "sponsor_id": None
        }
        partner.write(vals)
        print("Partner {} processed".format(partner_ref))
