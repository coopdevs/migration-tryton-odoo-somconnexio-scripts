import erppeek

client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")


def _filter_address_by_type(address_list, address_type):
    # If more than one same type address is found, pick the first one
    # (even if with invoice cases there will never be more than one
    return next((address for address in address_list
                 if address["type"] == address_type), None)


def _get_partner_address_to_assign(partner):

    partner_related_addresses = Partner.browse(
        [
            ("parent_id", "=", partner.id),
            ("type", "in", ("invoice", "delivery", "service", "other")),
        ],
        order='id DESC'
    ).read('type state_id city zip street street2')

    if len(partner_related_addresses) == 0:
        return False

    elif len(partner_related_addresses) == 1:
        return partner_related_addresses[0]

    invoice_address = _filter_address_by_type(
        partner_related_addresses, "invoice")

    if invoice_address:
        return invoice_address

    service_address = _filter_address_by_type(
        partner_related_addresses, "service")

    if service_address:
        return service_address

    other_address = _filter_address_by_type(
        partner_related_addresses, "other")

    if other_address:
        return other_address

    delivery_address = _filter_address_by_type(
        partner_related_addresses, "delivery")

    if delivery_address:
        return delivery_address

    return False


partners_without_address = Partner.browse(
    [
        ("parent_id", "=", None),
        ("active", "=", True),
        '|', ("country_id", "=", None),
        ("state_id", "=", None),
    ],
)

for partner in partners_without_address:

    address = _get_partner_address_to_assign(partner)

    if address:
        try:
            partner.write(
                {
                    "country_id": 68, # Spain id in res_country model
                    "state_id": address["state_id"],
                    "city": address["city"],
                    "zip": address["zip"],
                    "street": address["street"],
                    "street2": address["street2"]
                }
            )

            print("Updated Partner: {}. Assigned address: {}".format(
                partner.id, address["type"]))

        except Exception as error:
            print("Could not update partner: {} because of exception: {}".format(
                partner.id, error.faultCode))
    else:
        print("Partner: {} does not have any related address to assign".format(
            partner.id))
