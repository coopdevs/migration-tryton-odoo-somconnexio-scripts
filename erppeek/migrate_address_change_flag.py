import erppeek
client = erppeek.Client.from_config('odoo')

CrmLeadLine = client.model("crm.lead.line")

crm_lead_lines = CrmLeadLine.browse([('broadband_isp_info', '!=', 'null')])

for crm_lead_line in crm_lead_lines:
    change_address = crm_lead_line.broadband_isp_info.change_address
    if change_address: 
        try:
            crm_lead_line.broadband_isp_info.write({
                "type": "location_change"
            })
        except Exception:
            print("Error with contract ID {}".format(crm_lead_line.id))
            traceback.print_exc()
        print("Migrated flag for crm lead line ID {}".format(crm_lead_line.id))
