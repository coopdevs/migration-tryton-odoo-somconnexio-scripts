import erppeek

client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")
ShareLine = client.model("share.line")

not_cooperator_partners = Partner.browse(
    [
        "|", 
            ("sc_cooperator_register_number", "=", None),
            ("sc_cooperator_register_number", "=", 0),
    ]
)


for partner in not_cooperator_partners:
    member_vals = {
        "member": False,
        "old_member": False,
        "cooperator_register_number": False
    }
    partner.write(
        member_vals
    )


    share_line = ShareLine.browse(
        [
            ("partner_id", "=", partner.id),
        ],
        limit=1
    )

    share_line.unlink()

    print("Fixed Partner: {}".format(partner.id))
