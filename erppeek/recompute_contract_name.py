import erppeek
import traceback
client = erppeek.Client.from_config('odoo')

Contract = client.model("contract.contract")

contracts = Contract.browse([('id','>',0)])

for contract in contracts:
    try:
        contract.write({
            "phone_number": contract.phone_number
        })
    except Exception:
        print("Error with contract ID {}".format(contract.id))
        traceback.print_exc()
    print("Recomputed name for contract ID {}".format(contract.id))
