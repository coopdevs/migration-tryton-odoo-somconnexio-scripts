import erppeek
import sys


try:
    move_id = sys.argv[1]
except:
    exit("""
ERROR! Move ID is required.

Run:
    python migrate_account_moves_payments_imported_tryton.py <Move ID>

    """)

client = erppeek.Client.from_config('odoo')

AccountMove = client.model("account.move")
AccountAccount = client.model("account.account")


move = AccountMove.get([("id", "=", move_id)])
account440 = AccountAccount.get([("code", "=", "44000001")])

new_move_data = {
    "journal_id": move.journal_id.id,
    "date": move.date,
    "ref": move.ref,
    "move_type": "other",
    "line_ids": []
}

def create_430(line):
    return {
        "account_id": line.account_id.id,
        "partner_id": line.partner_id.id,
        "debit": line.credit,
        "credit": line.debit,
    }

def create_440(line):
    return {
        "account_id": account440.id,
        "partner_id": line.partner_id.id,
        "credit": line.credit,
        "debit": line.debit,
    }

for line in move.line_ids:
    if line.account_id.code == "43000000":
        new_move_data["line_ids"].append((0, 0, create_430(line)))
        new_move_data["line_ids"].append((0, 0, create_440(line)))

new_move = AccountMove.create(new_move_data)

print(new_move.id)
