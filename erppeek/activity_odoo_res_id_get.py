import erppeek
import csv
from activity import Activity


def read_rows(filename):
    with open(filename) as csv_file:
        reader = csv.DictReader(csv_file)
        response = list(reader)
    return response


def get_codes_ids_dict(rows, model, ext_field):
    c = erppeek.Client.from_config('odoo')
    ext_ids_dict = {}
    ext_list = [row['res_id'] for row in rows]
    contract_codes_split = [ext_list[i:i + 100] for i in range(0, len(ext_list), 100)]
    for chunk_code in contract_codes_split:
        chunk_pair_list = c.model(model).read(
            [(ext_field, 'in', chunk_code)], ['id', ext_field]
        )
        for pair in chunk_pair_list:
            ext_ids_dict[pair[ext_field]] = pair['id']
    return ext_ids_dict


def write_rows_with_odoo_ids(rows, code_id_dict, filename):
    with open(filename, mode="w") as csv_file_write:
        writer = csv.DictWriter(csv_file_write, Activity.csv_fields)
        writer.writeheader()
        for row in rows:
            tryton_id = row['res_id']
            if tryton_id in code_id_dict:
                row['res_id'] = code_id_dict[tryton_id]
            else:
                continue
            writer.writerow(row)
