from activity_odoo_res_id_get import (
    get_codes_ids_dict, read_rows, write_rows_with_odoo_ids
)


rows = read_rows('../data/mail.activity-partner.csv')
contract_codes_ids = get_codes_ids_dict(rows, 'res.partner', 'ref')
write_rows_with_odoo_ids(rows, contract_codes_ids, '../data/mail.activity-partner_Odoo.csv')

