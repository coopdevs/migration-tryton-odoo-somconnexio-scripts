import erppeek

client = erppeek.Client.from_config('odoo')

MailActivity = client.model("mail.activity")

mail_activities = MailActivity.browse(
    [
        ("res_model", "=", "account.invoice"),
    ],
)

MailActivity.write([a.id for a in mail_activities], {"res_model": "account.invoice"})
