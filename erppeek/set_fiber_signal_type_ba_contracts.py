import erppeek
import csv

client = erppeek.Client.from_config('odoo')

Contracts = client.model("contract.contract")
FiberSignalType = client.model("fiber.signal.type")

fiber_signal_type_list = FiberSignalType.browse([('id','>',0)]).read('id code')

fiber_signal_type_dct = {}
for fiber_signal_type in fiber_signal_type_list:
    fiber_signal_type_dct[fiber_signal_type["code"]] = fiber_signal_type["id"]

with open('ba_contracts_fiber_signal_type.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        fiber_signal_code = row['code']
        contract_id = row['contract_id']
        phone_number = row['phone_number']

        print("Processing line: ", phone_number)
        fiber_signal_type_id = fiber_signal_type_dct.get(fiber_signal_code)

        if not fiber_signal_type_id:
            print("This fiber_signal_type is not in the database. ", fiber_signal_code)
            continue

        contract = Contracts.browse(
            [
                ('id', '=', contract_id),
                ('phone_number', '=', phone_number),
            ]
        )

        if not contract:
            print("This contract is not in the database. ", phone_number)
            continue

        contract.write({"fiber_signal_type_id": fiber_signal_type_id})

        print("Finished processing line: ", phone_number)
