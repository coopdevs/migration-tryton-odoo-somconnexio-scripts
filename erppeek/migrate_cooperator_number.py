import erppeek

import csv

client = erppeek.Client.from_config('odoo')

Partner = client.model("res.partner")
IrSequence = client.model("ir.sequence")

sequence_coop_number = IrSequence.browse(
    [
        ("code", "=", "subscription.register"),
    ],
    limit=1
)

# Read CSV and copy/consume the number in the sc_cooperator_register_number.
print("* Read CSV and copy/consume the number in the sc_cooperator_register_number.")
old_members_to_update = []
with open('old_members_without_cooperator_number.csv', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        old_members_to_update.append(row)

for old_member in old_members_to_update:
    print("Searching partner {}".format(old_member.get("ref")))
    partner = Partner.browse(
        [
            ("ref", "=", old_member.get("ref")),
            ("vat", "=", old_member.get("vat")),
        ],
        limit=1
    )
    if not partner:
        print("Partner not found. Ref: {}".format(old_member.get("ref")))
        continue
    partner = partner[0]
    if partner.sc_cooperator_register_number:
        continue

    coop_number = int(old_member.get("coop_number")) if old_member.get("coop_number") else None
    if not coop_number:
        coop_number = sequence_coop_number.next_by_id()
        print("Partner without Coop number. New assigned: {}".format(coop_number))
    print("Partner cooperator register number: {}".format(coop_number))
    partner.write({
        "sc_cooperator_register_number": coop_number
    })
    print("Partner updated {}".format(old_member.get("ref")))

# Search all partners with sc_cooperator_register_number without cooperator_register_number and copy it.
print("* Search all partners with sc_cooperator_register_number without cooperator_register_number and copy it.")
partners_without_coop_number = Partner.browse(
    [
        ("cooperator_register_number", "=", None),
        "|",
        ("sc_cooperator_register_number", "!=", None),
        ("sc_cooperator_register_number", "!=", 0),
    ],
)

print("Partners to update: {}".format(len(partners_without_coop_number)))
for partner in partners_without_coop_number:
    print("Updating partner: {}".format(partner.id))
    partner.write({
        "cooperator_register_number": partner.sc_cooperator_register_number,
    })
